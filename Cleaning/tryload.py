#
import json

file = 'usa-hospital-beds.geojson'
with open(file) as json_file:
    try: 
        data = json.load(json_file)
    except Exception as e:
        print(f"Cannot load {file} due to : {e}")