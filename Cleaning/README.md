# Cleaning Data


## Summary
Data in this repo has NOT been trimmed or adjusted.


## Getting Started

Review jupyter notebooks in the parent directory called "Cleaning Data # - (CONTENT).ipynb. 

These notebooks all download, import, and modify the data including some rudimentary descriptive statistics. 

Data in this directory creates customized files that are ready for import into JMP, see JMP [documentation here](https://www.jmp.com/en_us/offers/statistical-analysis-software.html?utm_source=google&utm_medium=cpc&utm_campaign=td70114000002KZJq&utm_term=statistical%20software&utm_content=US-TRIAL/STAT&gclid=Cj0KCQiAy579BRCPARIsAB6QoIZaPvwmzOyNg-QtCmN6MaNa-6SN8hiJWgc8rGgOhd5klykz6J23VSMaAgfuEALw_wcB).
