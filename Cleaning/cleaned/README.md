# CLEANED Data


## Summary
Data in this repo has been trimmed or adjusted. All root data can be found in the main folder unmodified. 


## Getting Started

Review jupyter notebooks in the parent directory called "Cleaning Data # - (CONTENT).ipynb. 

These notebooks all download, import, and modify the data including some rudimentary descriptive statistics. 

Data in this directory is going to be used in the ../Analysis directory and then in the ../Project directory to make statistical conclusions