# US state population data 
https://www.census.gov/data/tables/time-series/demo/popest/2010s-state-total.html  - Dataset - Population - full
http://www2.census.gov/programs-surveys/popest/datasets/2010-2019/national/totals/nst-est2019-alldata.csv

### Column explanation: 
https://www2.census.gov/programs-surveys/popest/technical-documentation/file-layouts/2010-2019/nst-est2019-alldata.pdf

Counties : https://www2.census.gov/programs-surveys/popest/tables/2010-2019/counties/totals/co-est2019-annres.xlsx


# GeoJson Data Hospital Beds - 
https://covid19-lake.s3.us-east-2.amazonaws.com/rearc-usa-hospital-beds/json/usa-hospital-beds.geojson

# BTS - statistics for driving information
https://www.bts.gov/archive/publications/highlights_of_the_2001_national_household_travel_survey/table_02

https://www.bts.gov/covid-19

COVID 19 Data : 
https://www.bts.gov/browse-statistical-products-and-data/trips-distance/daily-travel-during-covid-19-pandemic


General Data: 
https://www.bts.gov/sites/bts.dot.gov/files/legacy/publications/highlights_of_the_2001_national_household_travel_survey/csv/table_02.csv
https://www.bts.gov/archive/publications/highlights_of_the_2001_national_household_travel_survey/table_a08
