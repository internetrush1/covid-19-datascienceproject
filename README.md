# COVID19 Data Science Project

## Summary

This is a project to look at CENSUS data vs the current available COVID 19 data to answer questions specific to relative probability of getting ill or dead in any given state. 

Currently only reviewing WI, CA, and NY. 

## Getting started

Getting started involves installing the items in the requirements.txt file. 

Alternatively use anaconda https://docs.anaconda.com/anaconda/install/ which gives virtual environments and the pandas, numpy, and other utilities for free. 

